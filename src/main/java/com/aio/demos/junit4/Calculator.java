package com.aio.demos.junit4;

/**
 * Hello world!
 *
 */
class Calculator
{
    int multiply(int a, int b) {
        return a * b;
    }

    int add(int a, int b) {
        return a + b;
    }
}
