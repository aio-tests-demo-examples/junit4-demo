import com.aio.demos.junit4.CalculatorAddTest;
import com.aio.demos.junit4.CalculatorTest;
import com.aio.demos.junit4.RepeatedParametrizedTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({
        CalculatorTest.class,
        CalculatorAddTest.class,
        RepeatedParametrizedTest.class
})

public class AllSuite {

}
