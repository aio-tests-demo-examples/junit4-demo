package com.aio.demos.junit4;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CalculatorAddTest {

    private Calculator calculator;

    @Before
    public void setUp() {
        calculator = new Calculator();
    }

    @Test
    public void testAdd() {
        assertEquals( "Regular addition should work", calculator.add(4,5), 9);
    }

    @Test
    public void testAddWithOthers() {
        assertEquals("Add with zero should be same number",8,  calculator.add(0,8));
        assertEquals("Negative numbers should be negative", -4, calculator.add(-2,-2));
    }
}