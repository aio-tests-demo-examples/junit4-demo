package com.aio.demos.junit4;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

import java.util.Arrays;
import java.util.Collection;
import java.util.Random;

import static org.junit.Assert.assertEquals;

@RunWith(Parameterized.class)
public class RepeatedParametrizedTest {


    private int numberA;
    private int numberB;
    private int expected;
    private Calculator calculator = new Calculator();

    public RepeatedParametrizedTest(int numberA, int numberB, int expected) {
        this.numberA = numberA;
        this.numberB = numberB;
        this.expected = expected;
    }

    @Parameterized.Parameters
    public static Collection<Object[]> data() {
        Object[][] obj = new Object[10][3];
        Random r = new Random();
        for (int i = 0; i < 10; i++) {
            Integer[] insert = {r.nextInt(10), r.nextInt(10), r.nextInt(20)};
            obj[i] = insert;
        }
        return Arrays.asList(obj);
    }

    @Test
    public void test_addTwoNumbers() {
        assertEquals(calculator.add(numberA, numberB), expected);
    }

}
